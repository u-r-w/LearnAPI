//learn basic of routing
//http://expressjs.com/en/starter/basic-routing.html

var express    = require('express'); // require itu seperti import dimana disini kita mengimport express
var bodyParser = require('body-parser'); //middleware
var app        = express(); //instantiate to express supaya app dapat menggunakan query express
var router     = express.Router();  //query dari express yang memerlukan 2 hal(path, handler)
var port       = process.env.PORT || 3000  // membuat port untuk listener

//==> setting database
var mongoose = require('mongoose'); //mengambil query mongoDB supaya bisa dipakai dalam aplikasi kita
var promise = mongoose.connect('mongodb://localhost/apiUser', {
  useMongoClient: true,
  /* other options */
});

var User = require('./models/user')   // untuk mengimport user yang ada pada model 

//==> konfugurasi bodyParser
app.use(bodyParser.urlencoded({ extend: true}));
app.use(bodyParser.json())

//middleware
router.use(function(req, res, next){
  console.log('Time: ', Date.now());
  next();
})

//==> test router
router.get('/', function(req, res){ //
  res.json({message: 'anda di home'})
})

router.route('/users').post(function(req, res){
  var user      = new User();
  user.name     = req.body.name;
  user.password = req.body.password;

  user.save(function(err){
    if(err) res.send(err);

    res.json({message: "user berhasil dimasukkan"})
  });

}).get(function(req, res) {
  User.find(function(err, users){
    if(err) res.send(err)

    res.json({users})
  })
});

router.route('/users/:name')
    .get(function(req, res){
      User.find({name: req.params.name}, function(err, user){
        if(err) res.send(err)
        res.json(user);
      })
    }).put(function(req, res){
      User.update(
        { name: req.params.name },
        { name: req.body.name },
        function(err, user) {
          if (err) res.send(err);
          res.json('user berhasil di update')
        }
      )
    }).delete(function(req, res) {
      User.remove({
        name: req.params.name
      }, function(err, user){
        if(err) res.send(err)
        res.json({message: 'user berhasil di delete'})
      })
    });

//==> prefix api
app.use('/api', router);

app.listen(port);
console.log('port run on ' + port);
